//
//  Service.swift
//  denemeTask
//
//  Created by Hakan Üstünbaş on 25.03.2021.
//

import Foundation

class Service {
    
    var mentions  = [Mention]()
    var users = [Person]()

    func createMentions() -> [Mention]  {
        
        let onur = Person(profileImage: "", userName: "Onur Basdas", userJob: "iOS Developer")
        let hakan = Person(profileImage: "", userName: "Hakan Ustunbas", userJob: "iOS Developer")
        let ekrem = Person(profileImage: "", userName: "Ekrem Ozkaraca", userJob: "iOS Developer")
        
        users.append(onur)
        users.append(hakan)
        users.append(ekrem)
        
        let mention1 = Mention(userMention: "Asdasfasafasasfasafas", mentionImage: "https://images.prismic.io/prospects-ac-uk/8edf378671893d64db90eba19a886c098b106553_work-in-new-zealand-1.jpg?auto=compress,format", mentionTime: "12 Mart 2021", userLikeMention: false, userPosted: users[0])
        let mention2 = Mention(userMention: "pğoğoğopğopğopoppopppo", mentionImage: "", mentionTime: "12 Mart 2021", userLikeMention: false, userPosted: users[1])
        let mention3 = Mention(userMention: "Asdasfasafasasfasafas", mentionImage: "https://www.bettertechtips.com/wp-content/uploads/2017/04/raw-file.jpg", mentionTime: "12 Mart 2021", userLikeMention: false, userPosted: users[2])
        
        mentions.append(mention1)
        mentions.append(mention2)
        mentions.append(mention3)
        
        return mentions
    }
}
