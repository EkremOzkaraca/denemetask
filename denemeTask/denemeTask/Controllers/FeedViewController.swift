//
//  FeedViewController.swift
//  denemeTask
//
//  Created by Ekrem Özkaraca on 25.03.2021.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let imageArray = [UIImage(named: "nature"), UIImage(named: "foto"), UIImage(named: "nature"), UIImage(named: "foto")]
    let mentionArray = ["Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release", "of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."]
    let nameArray = ["Lebron James", "Micheal Jordan", "Kevin Durant", "A.Davis"]
    let jobArray = ["Hr Reps", "ios Dev", "Android dev", "Managagement information System specialist"]
    
    
    @IBOutlet weak var feedTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView.delegate = self
        feedTableView.dataSource = self
        let nib = UINib(nibName: "FeedTableViewCell", bundle: nil)
        feedTableView.register(nib, forCellReuseIdentifier: "FeedTableViewCell")
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        cell.conf()
        cell.profileImageView.image = UIImage(named: "profile")
        cell.jobLabel.text = jobArray[indexPath.row]
        cell.mentionLabel.text = mentionArray[indexPath.row]
        cell.nameLabel.text = nameArray[indexPath.row]
        cell.mentionImageView.image = imageArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetailsVC", sender: nil)
    }
}
