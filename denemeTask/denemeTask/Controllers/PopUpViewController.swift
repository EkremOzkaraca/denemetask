//
//  PopUpViewController.swift
//  denemeTask
//
//  Created by Onur Başdaş on 25.03.2021.
//

import UIKit

class PopUpViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    @IBOutlet var labelText: UILabel!
    @IBOutlet var popUpView: UIView!
    @IBOutlet var collectionPop: UICollectionView!
    @IBOutlet var favButton: UIButton!
    
    var isCouponFav = UserDefaults.standard.bool(forKey: "isCouponFav")
    let arrButtons = ["#3Pounds","#employee","#recognition","#influential","#executive","#advocacy","#organization","#marketing"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.collectionPop.register(UINib(nibName:"PopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        collectionPop.delegate = self
        collectionPop.dataSource = self
        favButton.tag = 0
        labelText.numberOfLines = 0
        labelText.text = "Capturing that attention can be more difficult for brands than individuals. So getting your employees to share articles from your blog, pictures around the office or other types of content can significantly improve your reach. We’ll dive more into how and why this works in the next few sections.\n\nPeople crave recognition. Taking the time to recognize employees who perform well can also be extremely beneficial for business.\n\nIn the past, many companies took a cautious approach to employee social activity due to compliance and legal concerns. But times have changed and organizations today understand the marketing potential of the advocates within their companies. By leveraging the networks your employee advocates built out, and your brand can broaden its social reach and present its messaging more authentically to its audience. People build stronger connections with other people than they do with brands, which is why they are more receptive to your employees than your brand's account. People tend to view a brand account or even a C-level executive's account the same as they view an ad, which is why employees are your most influential resource. They don't receive the level of skepticism that a brand account, executive or ad will receive, and become micro-influencers of your industry that your organization can leverage. #3pounds  "
        
        popUpView.layer.cornerRadius = 15
        popUpView.layer.masksToBounds = true
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! PopCollectionViewCell
        cell.btnButton?.setTitle(arrButtons[indexPath.row], for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrButtons.count
    }
    
    @IBAction func sharedClicked(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: [self.labelText.text as Any], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func favoriteClicked(_ sender: Any) {
        
        if (favButton.tag == 0) {
            let image = UIImage(named: "starselected.png")
            (sender as AnyObject).setImage(image, for: .normal)
            makeAlert(titleInput: "Favorite Change", messageInput: "Your select favorite")
            favButton.tag = 1
        } else {
            
            let image = UIImage(named: "star.png")
            (sender as AnyObject).setImage(image, for: .normal)
            makeAlert(titleInput: "Favorite Change", messageInput: "Your select unfavorite")
            favButton.tag = 0
        }
    
        UserDefaults.standard.set(isCouponFav, forKey: "isCouponFav")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first
        if touch?.view != self.popUpView
        { self.dismiss(animated: true, completion: nil) }
    }
    
    func makeAlert(titleInput: String, messageInput: String){
        let alert = UIAlertController(title: titleInput, message: messageInput, preferredStyle: UIAlertController.Style.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }

}
