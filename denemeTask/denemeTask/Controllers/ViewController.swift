//
//  ViewController.swift
//  denemeTask
//
//  Created by Onur Başdaş on 25.03.2021.
//

import UIKit
import ActiveLabel



class ViewController: UIViewController {
    @IBOutlet var labelText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label = ActiveLabel.init()
        label.numberOfLines = 4
        label.frame = CGRect(x: 10, y: 100, width: 300, height: 500)
        label.customize { label in
            label.text = "This is a post with #multiple #hashtags and a @userhandle. https://www.google.com/"
            label.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
            label.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            label.mentionColor = UIColor(red: 238.0/255, green: 85.0/255, blue: 96.0/255, alpha: 1)
//            label.URLColor = UIColor(red: 85.0/255, green: 238.0/255, blue: 151.0/255, alpha: 1)
            label.URLColor = .purple
            label.handleMentionTap { _ in print("Mention") }
            label.handleHashtagTap { _ in print("Hashtag") }
            label.handleURLTap { _ in self.performSegue(withIdentifier: "toDet", sender: nil) }
        }
        self.view.addSubview(label)
        labelText.text = label.text
        labelText.numberOfLines = 0
    }
    
    
}
