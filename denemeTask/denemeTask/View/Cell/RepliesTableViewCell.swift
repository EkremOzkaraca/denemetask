//
//  RepliesTableViewCell.swift
//  denemeTask
//
//  Created by Ekrem Özkaraca on 26.03.2021.
//

import UIKit

class RepliesTableViewCell: UITableViewCell {

    @IBOutlet weak var repliesImageView: UIImageView!
    @IBOutlet weak var repliesNameMention: UILabel!
    @IBOutlet weak var repliesJobLabel: UILabel!
    @IBOutlet weak var repliesDateLabel: UILabel!
    @IBOutlet weak var repliesMentionLabel: UILabel!
    @IBOutlet weak var repliesNameReply: UILabel!
    @IBOutlet weak var repliesReplyLabel: UILabel!
    
    static let identifier = "RepliesTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "RepliesTableViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
