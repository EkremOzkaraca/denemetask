//
//  FeedTableViewCell.swift
//  denemeTask
//
//  Created by Ekrem Özkaraca on 25.03.2021.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var mentionLabel: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var mentionImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mentionImageView.layer.borderWidth = 2.0
        mentionImageView.contentMode = .center
        mentionImageView.layer.masksToBounds = false
        mentionImageView.layer.borderColor = UIColor.white.cgColor
        mentionImageView.clipsToBounds = true
        
        profileImageView.layer.borderWidth = 5.0
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        profileImageView.clipsToBounds = true
    }
    
    func conf() {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .none
        selectedBackgroundView = backgroundView
        cartView.layer.shadowColor = UIColor.gray.cgColor
        cartView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cartView.layer.shadowOpacity = 1.0
        cartView.layer.masksToBounds = false
        cartView.layer.cornerRadius = 15.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
