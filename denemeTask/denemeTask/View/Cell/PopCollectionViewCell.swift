//
//  PopCollectionViewCell.swift
//  denemeTask
//
//  Created by Onur Başdaş on 25.03.2021.
//

import UIKit

class PopCollectionViewCell: UICollectionViewCell {
    @IBOutlet var backView: UIView!
    @IBOutlet var btnButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 10
        
    }
    
}
