//
//  HashtagCell.swift
//  denemeTask
//
//  Created by Hakan Üstünbaş on 26.03.2021.
//

import UIKit

class HashtagCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userJob: UILabel!
    @IBOutlet weak var mentionLabel: UILabel!
    @IBOutlet weak var mentionImage: UIImageView!
    @IBOutlet weak var mentionTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 10
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
    }

}
