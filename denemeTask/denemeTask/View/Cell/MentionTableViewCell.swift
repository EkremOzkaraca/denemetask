//
//  MentionTableViewCell.swift
//  denemeTask
//
//  Created by Ekrem Özkaraca on 26.03.2021.
//

import UIKit

class MentionTableViewCell: UITableViewCell {

    @IBOutlet weak var mentionImageView: UIImageView!
    @IBOutlet weak var mentionNameLabel: UILabel!
    @IBOutlet weak var mentionJobLabel: UILabel!
    @IBOutlet weak var mentionMentionLabel: UILabel!
    @IBOutlet weak var mentionDateLabel: UILabel!
    
    
    static let identifier = "MentionTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "MentionTableViewCell", bundle: nil)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
