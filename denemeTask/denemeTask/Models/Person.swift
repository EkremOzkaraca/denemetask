//
//  Person.swift
//  denemeTask
//
//  Created by Hakan Üstünbaş on 25.03.2021.
//

import Foundation

struct Person {
    var profileImage : String?
    var userName : String?
    var userJob : String?
}
