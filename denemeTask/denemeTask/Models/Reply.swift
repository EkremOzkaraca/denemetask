//
//  Reply.swift
//  denemeTask
//
//  Created by Hakan Üstünbaş on 25.03.2021.
//

import Foundation

struct Reply {
    var userReply : String?
    var replyTime : String?
    var userReplied : Person?
    var mentionReply : Mention?
}
