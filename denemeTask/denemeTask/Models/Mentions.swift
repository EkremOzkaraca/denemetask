//
//  Mentions.swift
//  denemeTask
//
//  Created by Hakan Üstünbaş on 25.03.2021.
//

import Foundation

struct Mention {
    var userMention : String?
    var mentionImage : String?
    var mentionTime : String?
    var userLikeMention : Bool?
    var userPosted : Person?
}
